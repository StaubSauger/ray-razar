# Ray Razar
A little personal Ray Tracer.

![Alt text](/Screenshots/1.png?raw=true "Screenshot")

What works?
* Basic Tracing / Diffuse / Specular Reflection
* Multiple Light Sources / Objects
* Yaw/Pitch/Roll Transformation

What does not work (yet)?
* Integration of diffuse Reflection
* Transmissive Materials
* Anti-Aliasing
* Multi-Threading with Rayon
