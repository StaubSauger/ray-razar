mod vector;
mod ray;

pub mod body;

pub use vector::{Vector, YawPitchRollMatrix};
pub use ray::Ray;
pub use crate::vision::Color;


// pub type Color = sdl2::pixels::Color;

#[derive(Copy, Clone)]
pub struct ReflectionVectors{
    pub point: Vector,
    pub n: Vector,
    pub r: Option<Ray>,
    // pub t: Ray,
}


pub trait Intersect {
    fn intersect(&self, ray: &Ray) -> Option<(Vector, Vector)>;
}
