use core::fmt;

use crate::geometry::{Color, Intersect, Ray, ReflectionVectors, Vector};

use super::{angle, Reflectable, Triangle};
use crate::vision::ColorConfig;

pub struct Cube<'a> {
    faces: [Triangle; 12],
    color_config: ColorConfig<'a, Self>,
}

impl<'a> Cube<'a> {
    pub fn new(
        p1: Vector,
        p2: Vector,
        p3: Vector,
        p4: Vector,
        color_config: ColorConfig<'a, Self>,
    ) -> Option<Cube<'a>> {
        if p1 == p2 || p2 == p3 || p1 == p3 {
            return None;
        }
        let help2 = p2 - p1;
        let help3 = p3 - p1;
        let help4 = p4 - p1;

        let p5 = p2 + help3;
        let p6 = p2 + help4;
        let p7 = p3 + help4;
        let p8 = p1 + help2 + help3 + help4;

        let t1 = Triangle::new(p1, p2, p3);
        let t2 = Triangle::new(p1, p2, p4);
        let t3 = Triangle::new(p1, p3, p4);
        let t4 = Triangle::new(p3, p5, p2);
        let t5 = Triangle::new(p3, p4, p7);
        let t6 = Triangle::new(p6, p2, p4);
        let t7 = Triangle::new(p3, p5, p7);
        let t8 = Triangle::new(p8, p5, p7);
        let t9 = Triangle::new(p5, p2, p6);
        let t10 = Triangle::new(p5, p8, p6);
        let t11 = Triangle::new(p7, p4, p8);
        let t12 = Triangle::new(p6, p4, p8);
        Some(Cube {
            faces: [t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12],
            color_config,
        })
    }
}

impl<'a> fmt::Debug for Cube<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple("").field(&self.faces).finish()
    }
}

impl<'a> Reflectable for Cube<'a> {
        fn intersect(&self, ray: &crate::geometry::Ray) -> Option<ReflectionVectors> {
                let mut closest: Option<Vector> = None;
                let mut closest_triangle: Option<&Triangle> = None;
                let mut c_n: Option<Vector> = None;
                for triangle in &self.faces[0..12] {
                        if let Some((p, n)) = triangle.intersect(ray) {
                                if let Some(_) = closest {
                                }
                                if let None = closest {
                                        closest = Some(p);
                                        closest_triangle = Some(&triangle);
                                        c_n = Some(n);
                                } else if closest.unwrap().square_distance(&ray.origin)
                                        > p.square_distance(&ray.origin)
                                {
                                        closest_triangle = Some(&triangle);
                                        closest = Some(p);
                                        c_n = Some(n);
                                }
                        }
                }
                if let None = closest {
                        return None;
                }
                let closest_point = closest.unwrap();
                let closest_triangle = closest_triangle.unwrap();
                return Some(ReflectionVectors {
                        point: closest_point,
                        n: c_n.unwrap(),
                        r: closest_triangle.reflection_angle(closest_point, ray),
                });
        }
        fn specular(&self, incoming_ray: &Ray, outgoing_ray: &Ray, n: &Vector, color: Color) -> Color {
                let angle = angle(incoming_ray, outgoing_ray);
                (self.color_config.specular)(angle, self, color, incoming_ray, outgoing_ray, n)
        }

        fn diffuse(&self, incoming_ray: &Ray, outgoing_ray: &Ray, n: &Vector, color: Color) -> Color {
                let angle = angle(incoming_ray, outgoing_ray);
                (self.color_config.diffuse)(angle, self, color, incoming_ray, outgoing_ray, n)
        }

        fn transmission_rays(&self, incoming_ray: &Ray, n: &Vector, p: &Vector) -> Option<Vec<(Ray, Color)>> {
                (self.color_config.transmission_angle)(incoming_ray, n, p)
        }
}
