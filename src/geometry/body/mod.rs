mod triangle;
mod cube;
mod light;
mod sphere;

pub use super::Color;
pub use triangle::Triangle;
pub use cube::Cube;
pub use light::Light;
pub use sphere::Sphere;

use super::{Ray, ReflectionVectors, Vector};


pub trait Reflectable{
    fn intersect(&self, ray: &Ray) -> Option<ReflectionVectors>;
    fn specular(&self, incoming_ray: &Ray, outgoing_ray: &Ray, n: &Vector, color: Color) -> Color;
    fn diffuse(&self, incoming_ray: &Ray, outoing_ray: &Ray, n: &Vector, color: Color) -> Color;
    fn transmission_rays(&self, incoming_ray: &Ray, n: &Vector, p: &Vector) -> Option<Vec<(Ray, Color)>>;
}



fn angle(incoming_ray: &Ray, outgoing_ray: &Ray) -> f64{
    let a = incoming_ray.origin - outgoing_ray.origin;
    let b = outgoing_ray.direction();

    let top = a * b;

    let bottom1 = (a.x).powi(2) + (a.y).powi(2) + (a.z).powi(2);
    let bottom2 = (b.x).powi(2) + (b.y).powi(2) + (b.z).powi(2);

    return (top / (bottom1.powf(0.5) * bottom2.powf(0.5))).acos();

}
