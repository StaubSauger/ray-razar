use crate::{geometry::{Ray, ReflectionVectors, Vector}, vision::ColorConfig};

use super::{angle, Color, Reflectable};

pub struct Sphere<'a> {
    center: Vector,
    radius: f64,
    radius2: f64,
    color_config: ColorConfig<'a, Self>,
}

impl<'a> Sphere<'a> {
    pub fn new(
        center: Vector,
        radius: f64,
        color_config: ColorConfig<'a, Self>
    ) -> Option<Sphere<'a>> {
        if radius <= f64::EPSILON * 10. {
            return None;
        }
        Some(Sphere {
            center,
            radius,
            radius2: radius * radius,
            color_config,
        })
    }
    fn reflection_angle(&self, p: Vector, ray: &Ray) -> Option<Ray> {
        let d = ray.origin - p;
        let n = (p - self.center).normalize();
        let direction = d - 2. * (d * n) * n;
        return Ray::new(p, -1. * direction).ok();
    }
}

impl<'a> Reflectable for Sphere<'a> {
    fn intersect(&self, ray: &Ray) -> Option<ReflectionVectors> {
        let a = ray.direction() * ray.direction();
        let b = 2. * (ray.direction() * (ray.origin - self.center));
        let c = (ray.origin - self.center) * (ray.origin - self.center) - self.radius2;

        if let Some((x0, x1)) = solve_quadratic(a, b, c) {
            let t;
            if x1 > 0. {
                t = x1;
            } else if x0 > 0. {
                t = x0;
            } else {
                return None;
            }
            let point = ray.origin + t * ray.direction();
            let point = point + (point - self.center) * 0.0000001;
            return Some(ReflectionVectors {
                point,
                n: point - self.center,
                r: self.reflection_angle(point, ray),
            });
        }

        return None;
    }
    fn specular(&self, incoming_ray: &Ray, outgoing_ray: &Ray, n: &Vector, color: Color) -> Color {
        let angle = angle(incoming_ray, outgoing_ray);
        (self.color_config.specular)(angle, self, color, incoming_ray, outgoing_ray, n)
    }

    fn diffuse(&self, incoming_ray: &Ray, outgoing_ray: &Ray, n: &Vector,  color: Color) -> Color {
        let angle = angle(incoming_ray, outgoing_ray);
        (self.color_config.diffuse)(angle, self, color, incoming_ray, outgoing_ray, n)
    }

    fn transmission_rays(&self, incoming_ray: &Ray, n: &Vector, p: &Vector) -> Option<Vec<(Ray, Color)>> {
        (self.color_config.transmission_angle)(incoming_ray, n, p)
    }
}

fn solve_quadratic(a: f64, b: f64, c: f64) -> Option<(f64, f64)> {
    let discr = b * b - 4. * a * c;
    // println!("Discr: {}", discr);
    if discr < 0. {
        return None;
    } else if discr == 0. {
        let x = -0.5 * b / a;
        return Some((x, x));
    } else {
        let q;
        if b > 0. {
            q = -0.5 * (b + discr.sqrt());
        } else {
            q = -0.5 * (b - discr.sqrt());
        }
        let x0 = q / a;
        let x1 = c / q;
        if x0 > x1 {
            return Some((x0, x1));
        } else {
            return Some((x1, x0));
        }
    }
}
