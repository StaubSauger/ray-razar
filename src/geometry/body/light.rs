use crate::geometry::{Color, Vector};

pub struct Light{
    pub origin: Vector,
    pub color: Color,
}

impl Light{
    pub fn new(position: Vector, color: Color) -> Light{
        Light{
            origin: position,
            color,
        }
    }
}
