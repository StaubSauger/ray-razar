use crate::geometry::{Intersect, Ray, Vector};

#[derive(Debug)]
pub struct Triangle {
    p1: Vector,
    p2: Vector,
    p3: Vector,
    n_normal: Vector,
    d: f64,
}

impl Triangle {
    pub fn new(p1: Vector, p2: Vector, p3: Vector) -> Triangle {
        let n = ((p2 - p1).cross_product(p3 - p1)).normalize();
        let d = -1. * p1 * n;
        let n_normal = n;
        Triangle {
            p1,
            p2,
            p3,
            d,
            n_normal,
        }
    }

    fn inside(&self, point: Vector) -> bool {
        // if compare(point.x, self.p1.x, self.p2.x, self.p3.x) {
        //     return false;
        // }

        // if compare(point.y, self.p1.y, self.p2.y, self.p3.y) {
        //     return false;
        // }

        // if compare(point.z, self.p1.z, self.p2.z, self.p3.z) {
        //     return false;
        // }


        let edge0 = self.p2 - self.p1;
        let c0 = point - self.p1;
        if self.n_normal * edge0.cross_product(c0) <= -f64::EPSILON * 1. {
            return false;
        }

        let edge1 = self.p3 - self.p2;
        let c1 = point - self.p2;

        if self.n_normal * edge1.cross_product(c1) <= -f64::EPSILON * 1.{
            return false;
        }

        let edge2 = self.p1 - self.p3;
        let c2 = point - self.p3;

        if self.n_normal * edge2.cross_product(c2) <= -f64::EPSILON * 1.{
            return false;
        }

        return true;
    }

    pub fn reflection_angle(&self, p: Vector, ray: &Ray) -> Option<Ray> {
        let d = ray.origin - p;
        let direction = d - 2. * (d * self.n_normal) * self.n_normal;
        return Ray::new(p, -1. *  direction).ok();
    }
}

impl Intersect for Triangle {
    fn intersect(&self, ray: &Ray) -> Option<(Vector, Vector)> {
        let denominator = self.n_normal * ray.direction();
         if denominator.abs() < f64::EPSILON * 3.{
            return None;
         }
        let numerator = self.n_normal * ray.origin + self.d;

        let t = -numerator / denominator;
        if t <= f64::EPSILON * 2. {//2
            return None;
        }

        if (ray.direction() * t).abs_sum() < 0.001 {
            return None;
        }


        let point = ray.origin + ray.direction() * t;

        if !self.inside(point) {
            return None;
        }

        return Some((point, self.n_normal));
    }
}

fn compare(p: f64, c1: f64, c2: f64, c3: f64) -> bool {
    todo!();
    // (p > c1 && p > c2 && p > c3) || (p < c1 && pm < c2 && pm < c3)
}
