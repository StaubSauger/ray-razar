use super::body::Reflectable;
use super::{body::Light, Color, Intersect, ReflectionVectors, Vector};

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    pub origin: Vector,
    direction: Vector,
}

impl Ray {
    pub fn new(origin: Vector, direction: Vector) -> Result<Ray, &'static str> {
        let zero_v = Vector::from_single(0.);
        if zero_v == direction {
            return Err("Direction of ray cant be zero.");
        }
        Ok(Ray {
            origin,
            direction: direction.normalize(),
        })
    }
    pub fn direction(&self) -> Vector {
        self.direction
    }
    pub fn set_direction(&mut self, direction: Vector) -> Result<(), &str> {
        let zero_v = Vector::from_single(0.);
        if zero_v == direction {
            return Err("Direction of ray cant be zero.");
        }
        self.direction = direction;
        Ok(())
    }

    pub fn trace(
        &self,
        object_hit: &dyn Reflectable,
        object_list: &Vec<&dyn Reflectable>,
        light_list: &Vec<Light>,
        hops_left: i32,
        random: u32,
        self_angels: &ReflectionVectors,
        normal: &Vector,
        self_position: &Vector,
    ) -> Color {
        let mut self_color = Color::BLACK;
        // First check direct lighting

        // Check specular reflection
        if hops_left >= 0 {
            if let Some(new_ray) = self_angels.r {
                let incoming = new_ray.camera_trace(object_list, light_list, hops_left - 1, 0);
                self_color += object_hit.specular(self, &new_ray, normal, incoming);
            }
        }

        //Now Transmission
        if hops_left >= 0 {
            if let Some(transmissions) = object_hit.transmission_rays(self, normal, self_position) {
                for transmission in transmissions {
                    let transmission_color =
                        transmission
                            .0
                            .camera_trace(object_list, light_list, hops_left - 1, random);

                    self_color += transmission_color * transmission.1;
                }
            }
        }

        // Check diffuse reflection
        if hops_left > 0 && random > 2 {
            // To do some kind of integration over the different possible vectors we use a trick.
            // First split the normal in to 2 lineary independant parts.
            let p1;
            let p2;
            let mut normal = *normal;
            if normal * self.origin > 0. {
                normal = normal * -1.;
            }
            if !(normal.y.abs() < 0.01 && normal.z.abs() < 0.01) {
                let x_axis = Vector::new(1., 0., 0.);
                p1 = x_axis.cross_product(normal);
                p2 = p1.cross_product(normal);
            } else {
                p1 = Vector::new(0., 1., 0.);
                p2 = Vector::new(0., 0., 1.);
            }

            let factor = (random - 1) * (random - 1) * (random - 1) - 1;
            // let factor = 1;
            for i in 0..random {
                for j in 0..random {
                    'inner: for z in 0..random {
                        let i = i as f64 / (random as f64 - 1.) * 2. - 1.;
                        let j = j as f64 / (random as f64 - 1.) * 2. - 1.;
                        let z = z as f64 / (random as f64 - 1.);

                        if i == 0. && j == 0. && z == 0. {
                            continue 'inner;
                        }
                        let new_direction = normal * z + p1 * i + p2 * j;
                        let new_ray = Ray::new(*self_position, new_direction).unwrap();
                        let incoming =
                            new_ray.camera_trace(object_list, light_list, hops_left - 1, 0);
                        self_color += incoming * (1. / factor as f64) * 0.25;
                    }
                }
            }
        }

        for light in light_list.iter() {
            let light_vector = Ray::new(*self_position, light.origin - *self_position).unwrap();
            let mut hit = true;
            for object in object_list.iter() {
                if let Some(p) = object.intersect(&light_vector) {
                    if light.origin.square_distance(self_position)
                        > light.origin.square_distance(&p.point)
                    {
                        hit = false;
                    }
                }
            }
            if hit {
                let outgoing_ray = Ray::new(*self_position, light.origin - *self_position).unwrap();
                self_color += object_hit.diffuse(self, &outgoing_ray, normal, light.color);
            }
        }

        return self_color;
    }

    pub fn camera_trace(
        &self,
        object_list: &Vec<&dyn Reflectable>,
        light_list: &Vec<Light>,
        hops_left: i32,
        random: u32,
    ) -> Color {
        let mut closest: Option<ReflectionVectors> = None;
        let mut object_hit = None;
        for object in object_list.iter() {
            if let Some(p) = object.intersect(self) {
                if let None = closest {
                    closest = Some(p);
                    object_hit = Some(object);
                } else {
                    if closest.clone().unwrap().point.square_distance(&self.origin)
                        > p.point.square_distance(&self.origin)
                    {
                        closest = Some(p);
                        object_hit = Some(object);
                    }
                }
            }
        }
        if let Some(p) = closest {
            return self.trace(
                *object_hit.unwrap(),
                object_list,
                light_list,
                hops_left,
                random,
                &p,
                &p.n,
                &p.point,
            );
        }
        return Color::BLACK;
    }
}
