use std::{cmp, ops};

#[derive(Clone, Copy, Debug)]
pub struct Vector {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

impl cmp::PartialEq<f64> for Vector {
    fn eq(&self, rhs: &f64) -> bool {
        self.x == *rhs && self.y == *rhs && self.z == *rhs
    }
}

impl cmp::PartialEq<Vector> for Vector {
    fn eq(&self, other: &Vector) -> bool {
        self.x == other.x && self.y == other.y && self.z == other.z
    }
}

impl ops::Mul<Vector> for Vector {
    type Output = f64;

    fn mul(self, rhs: Vector) -> Self::Output {
        self.x * rhs.x + self.y * rhs.y + self.z * rhs.z
    }
}

impl ops::Sub<Vector> for Vector {
    type Output = Vector;

    fn sub(self, rhs: Vector) -> Self::Output {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl ops::Add<Vector> for Vector {
    type Output = Vector;
    fn add(self, rhs: Vector) -> Self::Output {
        Vector {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl ops::Mul<f64> for Vector {
    type Output = Vector;
    fn mul(self, rhs: f64) -> Self::Output {
        Vector {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl ops::Div<f64> for Vector {
    type Output = Vector;
    fn div(self, rhs: f64) -> Self::Output {
        Vector {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs,
        }
    }
}


impl ops::Mul<Vector> for f64 {
    type Output = Vector;
    fn mul(self, rhs: Vector) -> Self::Output {
        rhs * self
    }
}

impl Vector {
    pub fn new(x: f64, y: f64, z: f64) -> Vector {
        Vector { x, y, z }
    }
    pub fn from_single(scalar: f64) -> Vector {
        Vector {
            x: scalar,
            y: scalar,
            z: scalar,
        }
    }

    pub fn cross_product(&self, rhs: Vector) -> Vector {
        Vector {
            x: self.y * rhs.z - self.z * rhs.y,
            y: self.z * rhs.x - self.x * rhs.z,
            z: self.x * rhs.y - self.y * rhs.x,
        }
    }

    pub fn yaw_pitch_rotate(&mut self, transformation_matrix: &YawPitchRollMatrix) {
        *self = *transformation_matrix * *self;
    }

    pub fn square_distance(&self, p: &Vector) -> f64 {
        (self.x - p.x).powi(2) + (self.y - p.y).powi(2) + (self.z - p.z).powi(2)
    }

    pub fn distance(&self, p: &Vector) -> f64 {
        self.square_distance(p).sqrt()
    }

    pub fn normalize(&self) -> Vector{
        *self / self.distance(&Vector::new(0.,0.,0.))
    }
    pub fn abs_sum(&self) -> f64{
        self.x.abs() + self.y.abs() + self.z.abs()
    }
}

#[derive(Clone, Copy, Debug)]
pub struct YawPitchRollMatrix {
    v1: Vector,
    v2: Vector,
    v3: Vector,
}

impl YawPitchRollMatrix {
    pub fn new(yaw: f64, pitch: f64, roll: f64) -> YawPitchRollMatrix {
        let v3 = Vector{
            x: -pitch.sin(),
            y: pitch.cos() * roll.sin(),
            z: pitch.cos() * roll.cos(),
        };

        let v2 = Vector{
            x: yaw.sin() * pitch.cos(),
            y: yaw.sin() * pitch.sin() * roll.sin() + yaw.cos() * roll.cos(),
            z: yaw.sin() * pitch.sin() * roll.cos() - yaw.cos() * roll.sin(),
        };

        let v1 = Vector{
            x: yaw.cos() * pitch.cos(),
            y: yaw.cos() * pitch.sin() * roll.sin() - yaw.sin() * roll.cos(),
            z: yaw.cos() * pitch.sin() * roll.cos() + yaw.sin() * roll.sin(),
        };


        println!("YawPitchRollMatrix:\n1. Line : {:?}\n1. Line : {:?}\n1. Line : {:?}", v1, v2, v3);

        YawPitchRollMatrix { v1, v2, v3 }
    }
}

impl ops::Mul<Vector> for YawPitchRollMatrix {
    type Output = Vector;

    fn mul(self, rhs: Vector) -> Self::Output {
        let v1 = Vector::new(self.v1.x * rhs.x, self.v1.y * rhs.x, self.v1.z * rhs.x);
        let v2 = Vector::new(self.v2.x * rhs.y, self.v2.y * rhs.y, self.v2.z * rhs.y);
        let v3 = Vector::new(self.v3.x * rhs.z, self.v3.y * rhs.z, self.v3.z * rhs.z);

        println!("YawPitchRollMatrix Multiplaction:\n1. Line : {:?}\n1. Line : {:?}\n1. Line : {:?}", v1, v2, v3);
        v1 + v2 + v3
    }
}
