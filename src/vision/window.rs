use sdl2::{pixels::Color, render, video};

pub struct Window {
    canvas: render::Canvas<video::Window>,
}

impl Window {
    pub fn new(resolution: (u32, u32)) -> Window {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();
        let window = video_subsystem
            .window("Ray Razar", resolution.0, resolution.1)
            .position_centered()
            .build()
            .map_err(|e| e.to_string()).unwrap();

        let mut canvas = window
            .into_canvas()
            .target_texture()
            .present_vsync()
            .build()
            .map_err(|e| e.to_string()).unwrap();

        Window{
            canvas,
        }
    }

    pub fn draw_pixel(&mut self, location: (i32, i32), color: Color) -> Result<(), String> {
        self.canvas.set_draw_color(color);
        let p = sdl2::rect::Point::new(location.0, location.1);
        if let Err(x) = self.canvas.draw_point(p) {
            return Err(x);
        }
        return Ok(());
    }

    pub fn present(&mut self) {
        self.canvas.present();
    }
}
