use std::ops;

use crate::geometry::{Ray, Vector};

#[derive(Clone, Copy, Debug)]
pub struct Color {
    r: f64,
    b: f64,
    g: f64,
}

impl Color {
    pub fn into_sdl(&self) -> sdl2::pixels::Color {
        sdl2::pixels::Color::RGB(max_255(self.r), max_255(self.g), max_255(self.b))
    }

    pub const BLACK: Color = Color {
        r: 0.,
        b: 0.,
        g: 0.,
    };

    pub const RED: Color = Color {
        r: 1.,
        b: 0.,
        g: 0.,
    };
    pub const GREEN: Color = Color {
        r: 0.,
        b: 0.,
        g: 1.,
    };
    pub const BLUE: Color = Color {
        r: 0.,
        b: 1.,
        g: 0.,
    };

    pub const WHITE: Color = Color {
        r: 1.,
        b: 1.,
        g: 1.,
    };

    pub const YELLOW: Color = Color {
        r: 1.,
        g: 1.,
        b: 0.,
    };
}

impl ops::AddAssign<Color> for Color {
    fn add_assign(&mut self, rhs: Color) {
        self.r += rhs.r;
        self.b += rhs.b;
        self.g += rhs.g;
    }
}

impl ops::Mul<f64> for Color {
    type Output = Color;

    fn mul(self, rhs: f64) -> Color {
        Color {
            r: self.r * rhs,
            b: self.b * rhs,
            g: self.g * rhs,
        }
    }
}

impl ops::Mul<Color> for Color {
    type Output = Color;

    fn mul(self, rhs: Color) -> Color {
        Color {
            r: self.r * rhs.r,
            b: self.b * rhs.b,
            g: self.g * rhs.g,
        }
    }
}

impl ops::Add<Color> for Color {
    type Output = Color;

    fn add(self, rhs: Color) -> Color {
        Color {
            r: self.r + rhs.r,
            b: self.b + rhs.b,
            g: self.g + rhs.g,
        }
    }
}

pub fn max_255(input: f64) -> u8 {
    let int = (input * 255.0) as i32;
    if int > 255 {
        return 255;
    }
    return int as u8;
}

pub struct ColorConfig<'a, T> {
    pub specular: &'a dyn Fn(f64, &T, Color, &Ray, &Ray, &Vector) -> Color,
    pub diffuse: &'a dyn Fn(f64, &T, Color, &Ray, &Ray, &Vector) -> Color,
    pub transmission_angle : &'a dyn Fn(&Ray, &Vector, &Vector) -> Option<Vec<(Ray, Color)>>,
}
