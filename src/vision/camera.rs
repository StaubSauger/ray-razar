use crate::geometry::{Ray, Vector, YawPitchRollMatrix};
use std::f64::consts::PI;

#[derive(Debug, Clone, Copy)]
pub struct Camera {
    origin: Vector,
    ul: Vector,
    ur: Vector,
    ll: Vector,
    lr: Vector,
    resolution: (u32, u32),
}

impl Camera {
    /// Default direction is the direction of the Z-Axis
    pub fn new(
        mut origin: Vector,
        resolution: (u32, u32),
        viewing_angle: (f64, f64),
        rotation: &YawPitchRollMatrix,
    ) -> Result<Camera, &str> {
        if resolution.0 <= 1 || resolution.1 <= 1 {
            return Err("Resolution has to be bigger than 1,1");
        }
        if viewing_angle.0 >= PI / 2. || viewing_angle.0 <= 0. {
            return Err("X-Viewing Angle should be between 0 (<) and (>=) Pi");
        }
        if viewing_angle.0 >= PI / 2. || viewing_angle.0 <= 0. {
            return Err("X-Viewing Angle should be between 0 (<) and (>=) Pi");
        }

        let mut ul = Vector::new(-viewing_angle.0.sin(), viewing_angle.1.sin(), 1. + origin.z);
        ul.yaw_pitch_rotate(&rotation);
        let mut ur = Vector::new(viewing_angle.0.sin(), viewing_angle.1.sin(), 1. + origin.z);
        ur.yaw_pitch_rotate(&rotation);
        let mut ll = Vector::new(-viewing_angle.0.sin(), -viewing_angle.1.sin(), 1. + origin.z);
        ll.yaw_pitch_rotate(&rotation);
        let mut lr = Vector::new(
            viewing_angle.0.sin(),
            -viewing_angle.1.sin(),
            1. + origin.z,
        );
        lr.yaw_pitch_rotate(&rotation);

        origin.yaw_pitch_rotate(&rotation);

        println!("Camera Origin: {:?}", origin);
        println!("Upper-Left:    {:?}", ul);
        println!("Upper-Rigt:    {:?}", ur);
        println!("Lower-Left:    {:?}", ll);
        println!("Lower-Right:   {:?}", lr);
        Ok(Camera {
            origin,
            ul,
            ur,
            ll,
            lr,
            resolution,
        })
    }
}

impl<'a> IntoIterator for &'a Camera {
    type Item = Ray;
    type IntoIter = CameraIterator;
    fn into_iter(self) -> CameraIterator {
        CameraIterator {
            camera: self.clone(),
            x: 0,
            y: 0,
        }
    }
}

pub struct CameraIterator {
    camera: Camera,
    x: u32,
    y: u32,
}

impl Iterator for CameraIterator {
    type Item = Ray;
    fn next(&mut self) -> Option<Ray> {
        if self.y >= self.camera.resolution.1 {
            return None;
        }

        let x_left_mult = 1. - self.x as f64 / (self.camera.resolution.0 - 1) as f64;
        let x_right_mult = self.x as f64 / (self.camera.resolution.0 - 1) as f64;
        let y_upper_mult = 1. - self.y as f64 / (self.camera.resolution.1 - 1) as f64;
        let y_lower_mult = self.y as f64 / (self.camera.resolution.1 - 1) as f64;


        let target = self.camera.ul * x_left_mult * y_upper_mult
            + self.camera.ur * x_right_mult * y_upper_mult
            + self.camera.ll * x_left_mult * y_lower_mult
            + self.camera.lr * x_right_mult * y_lower_mult;

        let direction = target - self.camera.origin;

        if self.x >= (self.camera.resolution.0 - 1) {
            self.x = 0;
            self.y += 1;
        } else {
            self.x += 1;
        }
        let ray = Ray::new(self.camera.origin, direction).unwrap();

        Some(ray)
    }
}
