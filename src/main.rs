mod geometry;
mod vision;

use std::f64::consts::PI;

use geometry::body::Triangle;
use geometry::body::{Cube, Light, Sphere};
use geometry::Vector;
use geometry::YawPitchRollMatrix;
use geometry::{body::Reflectable, Ray};
use vision::Camera;
use vision::Window;
use vision::{Color, ColorConfig};

const RESOLUTION: (u32, u32) = (800, 800);

fn main() {
    let mut window = Window::new(RESOLUTION);

    let camera_origin = Vector::new(0., 0., -8.);
    let transformation = YawPitchRollMatrix::new(0.3, -0.3, PI / 4.);
    let camera = Camera::new(camera_origin, RESOLUTION, (0.4, 0.4), &transformation).unwrap();

    let c1 = Vector::new(-100., -100., 100.);
    let c2 = Vector::new(-100., 100., 100.);
    let c3 = Vector::new(-100., -100., 21.);
    let c4 = Vector::new(100., -100., 100.);

    let color_config = ColorConfig {
        specular: &floor_specular,
        diffuse: &floor_diffuse,
        transmission_angle: &non_transmission,
    };
    let floor = Cube::new(c1, c2, c3, c4, color_config).unwrap();

    let c1 = Vector::new(0., 0., 0.);
    let c2 = Vector::new(0., 1., 0.);
    let c3 = Vector::new(0., 0., 1.);
    let c4 = Vector::new(1., 0., 0.);
    let color_config = ColorConfig {
        specular: &white_specular,
        diffuse: &red_diffuse,
        transmission_angle: &non_transmission,
    };
    let cube = Cube::new(c1, c2, c3, c4, color_config).unwrap();

    let c1 = Vector::new(0., -0.1, 0.);
    let c2 = Vector::new(0., -1.1, 0.);
    let c3 = Vector::new(0., -0.1, -1.);
    let c4 = Vector::new(-1., -0.1, 0.);
    let color_config = ColorConfig {
        specular: &white_specular,
        diffuse: &green_diffuse,
        transmission_angle: &non_transmission,
    };
    let cube2 = Cube::new(c1, c2, c3, c4, color_config).unwrap();

    let c1 = Vector::new(0.4, 0.4, -0.4);
    let c2 = Vector::new(0.4, 0.6, -0.4);
    let c3 = Vector::new(0.4, 0.4, -4.5);
    let c4 = Vector::new(0.6, 0.4, -0.4);
    let color_config = ColorConfig {
        specular: &green_specular_m,
        diffuse: &green_diffuse_m,
        transmission_angle: &non_transmission,
    };
    let cube3 = Cube::new(c1, c2, c3, c4, color_config).unwrap();

    let c1 = Vector::new(1.5, -0.5, -2.);
    let c2 = Vector::new(1.5, -1.5, -2.);
    let c3 = Vector::new(0.5, -0.5, -2.);
    let c4 = Vector::new(1.5, -0.5, -3.);
    let color_config = ColorConfig {
        specular: &white_specular,
        diffuse: &blue_diffuse,
        transmission_angle: &non_transmission,
    };
    let cube4 = Cube::new(c1, c2, c3, c4, color_config).unwrap();

    let color_config = ColorConfig {
        specular: &white_specular_s,
        transmission_angle: &non_transmission,
        diffuse: &yellow_diffuse_s,
    };
    let sphere = Sphere::new(Vector::new(1.3, -0.2, 0.), 0.5, color_config).unwrap();

    let color_config = ColorConfig {
        specular: &white_specular_s,
        transmission_angle: &non_transmission,
        diffuse: &yellow_diffuse_s,
    };
    let sphere2 = Sphere::new(Vector::new(1., -1., 4.), 2., color_config).unwrap();

    let color_config = ColorConfig {
        specular: &s_specular_t,
        transmission_angle: &s_transmission_t,
        diffuse: &s_diffuse_t,
    };
    let sphere_t = Sphere::new(Vector::new(0., -2., -1.5), 0.8, color_config).unwrap();

    let light = Light::new(Vector::new(1.5, 2., -10.), Color::WHITE * 0.4);
    let light2 = Light::new(Vector::new(0., 2., -4.), Color::WHITE * 0.4);
    let light3 = Light::new(Vector::new(3., -2., -10.), Color::WHITE * 0.6);
    let light4 = Light::new(
        Vector::new(-1.5, 2., -8.),
        Color::BLUE * 0.3 + Color::WHITE * 0.4,
    );

    println!("Hello, world!");
    window.present();

    let body: Vec<&dyn Reflectable> = vec![
        &cube, &cube2, &cube3, &cube4, &sphere, &sphere2, &floor, &sphere_t,
    ];
    // let body: Vec<&dyn Reflectable > = vec![&floor];
    // let body: Vec<&dyn Reflectable> = vec![&floor];
    let lights = vec![light2, light, light3, light4];
    // let lights = vec![light3];

    for (i, ray) in camera.into_iter().enumerate() {
        let color = ray.camera_trace(&body, &lights, 5, 0);
        println!("{}", i);
        if i % 10000 == 0 {
            window.present();
            // break;
        }

        window
            .draw_pixel(
                (
                    (i % RESOLUTION.0 as usize) as i32,
                    (i / RESOLUTION.0 as usize) as i32,
                ),
                color.into_sdl(),
            )
            .unwrap();
    }
    window.present();
    println!("Finsihed");
    wait();
    window.present();
    wait();
}

fn wait() {
    let mut s = String::new();
    std::io::stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");
}

fn white_specular(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::WHITE * 0.2 * i_color
}
fn red_diffuse(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::RED * i_color * 0.6
}
fn green_diffuse(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::GREEN * i_color * 0.6
}

fn green_diffuse_m(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::GREEN * i_color * 0.1
}

fn green_specular_m(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::GREEN * i_color * 0.2 + Color::WHITE * i_color * 0.75
}

fn blue_diffuse(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    let top = *n * o_ray.direction();
    let bottom = n.square_distance(&Vector::new(0., 0., 0.))
        * o_ray.direction().square_distance(&Vector::new(0., 0., 0.));
    let angle = (top / bottom).abs().asin() * 2. / PI;
    Color::BLUE * i_color * 0.1 + Color::BLUE * i_color * 0.4 * angle
}

fn yellow_diffuse_s(
    angel: f64,
    _parent: &Sphere,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::YELLOW * i_color * 0.6
}
fn white_specular_s(
    angel: f64,
    _parent: &Sphere,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::WHITE * 0.7 * i_color
}

fn floor_specular(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::WHITE * 0.3 * i_color
}
fn floor_diffuse(
    angel: f64,
    _parent: &Cube,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    Color::WHITE * i_color * 0.1
}

fn non_transmission(_i_ray: &Ray, _n: &Vector, _p: &Vector) -> Option<Vec<(Ray, Color)>> {
    None
}

fn s_diffuse_t(
    angel: f64,
    _parent: &Sphere,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    if *n * i_ray.direction() > 0. {
        Color::BLACK
    } else {
        Color::WHITE * i_color * 0.1
    }
}

fn s_specular_t(
    angel: f64,
    _parent: &Sphere,
    i_color: Color,
    i_ray: &Ray,
    o_ray: &Ray,
    n: &Vector,
) -> Color {
    let top = *n * o_ray.direction();
    let bottom = n.square_distance(&Vector::new(0., 0., 0.))
        * o_ray.direction().square_distance(&Vector::new(0., 0., 0.));
    let angle = (top / bottom).abs().acos() * 2. / PI;
    if *n * i_ray.direction() > 0. {
        Color::BLACK
    } else {
        Color::WHITE * i_color * 0.8
    }
}
fn s_transmission_t(i_ray: &Ray, n: &Vector, p: &Vector) -> Option<Vec<(Ray, Color)>> {
    let mut nn: f64 = 0.95;

    let cosI = (*n * i_ray.direction()).abs();
    let sinT2 = nn * nn * (1. - cosI * cosI);

    if sinT2 > 1.0 {
        return None;
    }
    let cosT = (1.0 - sinT2).sqrt();
    let direction = nn * i_ray.direction() + (nn * cosI - cosT) * *n;
    return Some(vec![
        ((
            Ray::new(*p + 0.01 * direction, direction).unwrap(),
            Color::WHITE,
        )),
    ]);
}
